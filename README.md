# Experimental Design: Treatment Assignment and Power Calculation in (Field) Experiments 
Lecturer: Sebastian O. Schneider

When implementing a field experiment, time is often a scarce resource. As a result, the issues of power calculation and treatment assignment might not receive the attention they deserve. This, however, could render the whole study useless: In the worst case, wrong conclusions will be drawn from the study, for example about the non-existence of treatment effects, when in reality, the treatment groups are just not comparable to begin with, or the study is underpowered. In addition, the replicability of studies, as well as sub-sample analysis might also crucially depend on power and comparability of treatment groups. 
In this course, we will discuss use cases for, and consequences of, the different methods of treatment assignment, and learn how to implement them and deal with the consequences. The second part of the course is devoted to power analysis: After discussing the theory, we will have a look at some best practices. After taking this course, participants will hopefully never skip power analysis, be able to make an informed decision about treatment assignment in their own research, and implement it within a very short time, using the provided code. 

At least 4 weeks prior to the course, the GitLab repository will contain a list of readings and preparation details for example discussion. Participants should have Stata and R (with R Studio) installed.

## Updates
- General reading list added (see below). Please come back in the week prior to the course to check for further preparation details. 

## Day one: August 23rd

### Session I: Treatment Assignment 
August 23rd: 1:30pm – 2:45pm
-	When random treatment assignment is too risky
-	Alternative methods: When and how to use them
-	Treatment Assignment and implementation design 
-	How to account for treatment assignment during analysis
-	Short intro to randomization inference
-	Examples

Break 15 mins

### Session II: Power Analysis 
August 23rd: 3:00pm – 4:15pm
-	Minimum Detectable Effect Size 
-	The case of clustered assignment
-	Imperfect compliance
-	Examples

## Day two: August 24th 

### Session III: Practice Session: Treatment Assignment and Randomization Inference
August 24th: 1:30pm – 2:45pm

Break 15 mins

### Session IV: Practice Session: Power Analysis
August 24th: 3:00pm – 4:15pm

## General Readings
The following list of literature gives a good overview and introduction to field experimental work; in the course, only a sub-set of topics that are covered in these articles or books will be discussed. 
Reading this list is not a requirement, however, conducting field experiments without having consulted (one of) these sources is not advisable. 
- Athey, S. & Imbens, G. W. (2017). The econometrics of randomized experiments. In *Handbook of Field Experiments*.
- Frölich, M. & Sperlich, S. (2019). Impact Evaluation. 
- Imbens, G. W. & Rubin, D. B. (205). Causal inference for statistics, social, and biomedical sciences. 
- Duflo, E.; Glennerster, R. & Kremer, M. (2007). Using randomization in development economics research: A toolkit. In *Handbook of Development Economics*. 
